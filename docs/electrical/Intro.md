---
sidebar_position: 1
sidebar_label: Intro
---

### **What does electrical do?**
The electrical subteam is responsible for building and mainting the electrical board (e-board) and its components. The electrical team is the bridge between the programming and mechanical subteams. 
### **Preparing for the electrical test?**
To prepare for the electrical test, make sure to know the purpose of each of the parts of the e-board (detailed throughout this guide). It is crucial that you know what all these components do and how they are properly wired. Pay attention to many of the details, as it is important for you to know these by the back of your hand. 

##### **Pay special attention to:** 
* Wire gauges 
* Voltage and Amperage requirements for most components
  * Ignore the RSL (robot signal light)
* The wiring layout
  * ie. How would I wire the VRM (voltage regulator module)? 
* Purpose of each component
* Know the difference between the different motor controllers than we use (Spark Max, Victor SPX, and Talon SRX)  
* Pneumatics and the parts involved in it 
* Learn where "special" wires go
  * Know what uses PWM, CAN, ethernet, USB, etc and where to connect those parts 

##### **Do not worry about:** 
* Proper connecting techniques like stripping wires, shrink wrapping, inserting wires securely into the PDP, etc. We will teach this to you ourselves in person
* The laws of electronics or small individual pieces like resistors and capacitors, just focus on the **FRC provided parts**
* Jaguar Motors 
* SD540s 
* DMC 60s 

##### **Special Notes:** 
* The OpenMesh radio is honestly just a wi-fi router. Definetely study it but it should clear up some confusion. 
  * **HOWEVER,** this year (2024-2025) a new radio is being introduced called the VH-109 radio. 
  * Study both. 
* Research wire management techniques 
  * It does not have to specific to FRC, instead you can research wire management techniques from anywhere, for example see what electricians might do to manage/label wires. 
 

### **Resources:**
* If you have a question, you can search it up on google, and you will likely find a thread about a similar topic. **ChiefDelphi** is like the Reddict for FRC, so use it. 
* We created documentation for what we did last year, so you may want to check it out. [Team 2554: 2023-2024 Electrical Documentation](https://docs.google.com/document/d/1hptWhvVV93rBXKpBPH-Az7EU-bUj_KkNopbdVujhxlg/edit?pli=1)
* Team 2853 created their own documentation, and while there are parts that are outdated, or not useful for electrical, you can use it to have compiled information. This is their "[Electrical Bible](https://mililanirobotics.gitbooks.io/frc-electrical-bible/content/index.html)"
* Understand the components with [WPILib Docs “Hardware Component Overview”!](https://docs.wpilib.org/en/stable/docs/controls-overviews/control-system-hardware.html)
* Learn to  create an electrical board from scratch using [WPILib Docs “Introduction to Robot Wiring”](https://docs.wpilib.org/en/stable/docs/zero-to-robot/step-1/intro-to-frc-robot-wiring.html)
* Useful Videos
  * [FRC Control System Overview](https://www.youtube.com/watch?v=ftr6FPV_ntQ&list=PLCiztOZ1Er_UvRsW0Fp8btQn7N1UlhU3n)
    * If you still are confused here is [a longer video ](https://www.youtube.com/watch?v=GvtpIHAQKxs)
  * [FRC Control System overview (physical demonstration for better understanding)](https://www.youtube.com/watch?v=6LrAkH1Kszs)
  * [FRC Control System overview part 2 (physically wiring the E-board together)](https://www.youtube.com/watch?v=LURrrqis2cM)

