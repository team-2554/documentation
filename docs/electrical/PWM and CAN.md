---
sidebar_position: 5
sidebar_label: PWM and CAN
---


### **PWM**
* Stands for Pulse Width Modulation. 
* Uses three individual wires colored white, red, and black (signal, power, and ground)
* Provided an analog 
  * Consider an anlog signal to be like a knob, while digital signals are like a switch. The knob could be between fully off and on, while the switch can only be on and off. So PWM is similar to a knob, we can give a variable signal. 
* Some motor controllers use PWM, but genuinely don't worry about it, because we will just use CAN.
* Each device wired through PWM needs to be connected to a corresponding port on the PWM section of the RoboRIO.

![PWM wires](https://cdn11.bigcommerce.com/s-t3eo8vwp22/images/stencil/500x659/products/210/2364/REV-11-1130_PWM-Cable-2-Alicia-FINAL__80872.1636578352.png?c=2)


### **CAN (Controller Area Network)**
* Is a method of Serial communication
    * Like the I2C and the SPI bus in that regard
    * Serial communication really just means that both devices can "talk" to each other 
        * This is the reason we use CAN for motor controllers, because that way we can get information from the motor controller, like what is the motor's actual RPM, and stuff like that.  
* We use yellow and green wire for CAN 
  * Connect **Yellow to Yellow** and **Green to Green**
* We will start it at the PDH, and it will terminate at the roboRIO. 
* Daisy Chaining
  * This is how we connect all CAN devices. 
  * The CAN chain will start at the PDH, and it will go to the first device, for example the PCM, and then from the PCM it will go to a talon SRX, and so on and so forth, till it terminates at the roboRIO

Daisy chaining between motors would look like this: ![Daisy Chaining](https://docs.wpilib.org/en/stable/_images/daisychain.png)