---
sidebar_label: About FIRST/FRC
---

# About FIRST/FRC
This page describes the basics of the FIRST Robotics Competition (FRC) and includes various external resources.

---

FRC is an annual international robotics competition that involves nearly 1000 teams from around the world. The first FRC event occurred in 1992, and the competition has been renowned for decades since. 

<iframe width="50%" height="400px" src="https://www.youtube.com/embed/Jd29kzjclV0" title="About FIRST Robotics Competition (2021)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Learn more about the history of FIRST [here](https://www.firstinspires.org/about/history).

Every year, there is a new challenge, or "game", that FRC robots must be built to compete in. The game is released on kickoff day, which is usually early in January. At kickoff, full game documentation is made accessible as well as a game animation - the 2023 animation can be found [here](https://www.youtube.com/watch?v=0zpflsYc4PA). FRC matches are 3v3 with randomly chosen alliances, ultimately leading up to an Alliance Selection period in which the top 8 teams choose their alliances for the ensuing playoff/elimination matches. Match videos can be found online for your reference.

The goal of FRC is not to dominate and triumph over other teams. The format of the competition is specifically designed to prevent this. Instead, the competition is most rewarding to those who demonstrate the most engineering skill and self-improvement. Furthermore, there are other areas of the competition, such as awards, which involve less STEM ability and more professional and entrepreneurship skills, as well as outreach efforts. See the [Operations subteam](../operations/intro.md) documentation for more details.

FIRST also oversees other competitions/programs, such as the FIRST Tech Challenge (FTC) and FIRST Lego League (FLL).
