---
sidebar_label: About Team 2554
---

# About Team 2554

## Team Name
You may see our team referred to by several names. Officially, our team name is "The WarHawks". We also simply call ourselves the JPS Robotics Team or JPS Robotics depending on the context. However, you will also see us very frequently refer to ourselves as Team 2554 or see the number 2554 throughout our social media or physical items in our workshop. 2554 is our FRC team number.

Unsurprisingly, our official name, WarHawks, is derived from JP's mascot, the Hawk.

## Team History
Our team was established as a club at JPS in 2007 by Mr. Sekuler and Neel Belani, the first president. Mr. Sekuler has long since retired, and Mr. Kearney has been our advisor and mentor since 2013.

Since our inception, we have held many events and activities both within FRC and in our community. We have also received a number of recognitions and grants from businesses and politicians (most notably NJ Legislators Robert Karabinchak and Patrick J. Diegnan Jr.).

In 2021, we were a global semifinalist at the (virtual) championships for the FIRST Innovation Challenge for our invention, Visualeyes - meaning we were among the top 20 our of around 800 teams that year. Visualeyes was a hat containing cameras and a microprocessor that could locate nearby objects and provide specific verbal directions to a blind or visually impaired user. This received worldwide attention and we were even interviewed live on [Cheddar News](https://cheddar.com/media/first-team-warhawks-creates-device-to-help-aid-the-blind).

More information about our team, such as mentions of us in the media, can be found on our [website](https://jpsrobotics2554.org).

## Workshop & Storage Locations
We work in Mr. Kearney's room (138) near the Boys' Locker Room. This room contains a variety of tools, machines, and materials that are necessary for our work. Some of our things are stored in utility closets throughout the building. We also have a trailer parked outside the building that is used to transport our robot and pit items to events. Additional materials are stored in a shipping container located on school grounds.