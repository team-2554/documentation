---
sidebar_position: 1
slug: /
---

# Home

Here you'll find [electrical](./electrical/intro), [mechanical](./mechanical/intro),  [programming](./programming/intro), and [operations](./operations/intro) information that we have made public for the FRC Challenge. For prospective applicants, you should treat our documentation as a study guide and use it to prepare for the applications process.

We also highly recommend that you read through the General Info section, regardless of what subteam(s) you are applying to or are a member of.

Use the **left sidebar** to quickly navigate to documentation for each team.
