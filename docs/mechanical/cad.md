---
sidebar_label: CAD
---

# CAD (Computer-Aided Design)

---

Aside from working with hand/power tools, CAD (Computer-Aided Design) is a useful skill that enables the mechanical subteam (and electrical subteam) to digitally prototype parts and even the entire robot itself. CAD designs is also commonly used for 3D printing or fabrication.

### Onshape
We use Onshape frequently for CAD. It is professional-quality CAD software that can be used right in your browser. Onshape provides a [Learning Center](https://learn.onshape.com/) for beginners. The CAD Basics Learning Path can be found [here](https://learn.onshape.com/learning-paths/introduction-to-cad). When using Onshape, **make sure to sign up with your school email** so you can obtain an educational license. This will allow you to use many resources that would otherwise require payment for access.

This [Video Series](https://www.youtube.com/watch?v=pMWnsHpDlQE&list=PLxmrkna-ixrIQmsPR3MITi4Ru1bnMH4-l) also goes over some Onshape basics.