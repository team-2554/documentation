---
sidebar_label: Breakaway (2010)
sidebar_position: 7
---

# Breakaway (2010)
Our robot was named Bob this year. Frame dimensions 36" long and 24" wide. See diagrams under Planning for additional information.

## Task
[Game animation](https://www.youtube.com/watch?v=IEHAj3EmpMw)

## Picture
![image](/img/mechanical/breakaway/robot.jpg)
[Photo Source](https://www.deviantart.com/xxsoulhunterxx/art/Team-2554-s-Robot-Bob-164421156)

## Planning
![plans](/img/mechanical/breakaway/layout_1.png)

<iframe src="/img/mechanical/breakaway/2010-layout.pdf" style={{width: "100%", height: "600px"}} />