---
sidebar_label: Charged Up (2023)
sidebar_position: 1
---

# Charged Up Documentation

### Task
Game Animation [here](https://www.youtube.com/watch?v=0zpflsYc4PA).
- Maneuver around playing field
- Pick up and transport cones and cubes
- Place cones and cubes on platforms and poles of varying height
- Balance robot on a shifting platform

### Arm
#### Purpose
- Extend out to reach and grab and place cubes and cones
- Retract inward to fit within frame limits

#### Build Process
- Built from aluminum tubing (hollow rectangular metal pieces) of varying sizes.
- Largest area at the base, decreasing as it moves further out.
- Paracord ran through the hollow interior of the arm as means of retracting the arm.
- A motor at the base of the arm spooled the paracord, pulling the arm compact.
- Coiled metal strips which act as springs are placed at the end of each level to help shoot up the arm without any external forces.
- No extra mechanisms to extend arm

#### Setbacks
- Paracord experienced issues with knotting.
- Too heavy, put the robot off balance.
- Required removing material to lighten the load.

![](https://i.ibb.co/Hh9Fdww/image.png)

### Claw
#### Purpose
- Open and close to grab cubes and cones.
- Pivot to orient cones in a way that they rest on their base.

#### Build Process
- Several iterations were created, each utilizing different paths of movement.
- First iteration utilized a linear actuator to pull two sides of the claw together.
- Second iteration utilized a similar system, though the path of the claw "pincers" was angled.
- Third iteration utilized a system of a linear actuator parallel to the pincers, with two positions of pivot to convert the parallel motion to match the angles at which the arms would go in and out.
- Circular pads with baking sheets were used to orient cones correctly.

#### Setbacks
- Linear actuator movement would not translate properly in creating lateral motion of the pincers.
- Weight of the claw offset the center of gravity of the arm.
- Previous grip surfaces did not hold the cones/cubes tightly enough, letting them slip out.

![](https://i.ibb.co/bWYMQXv/image.png)

### Bumper
#### Purpose
- Provides a safe cushioning for collisions with other robots, field objects, and barriers.

#### Build Process
- Started with measuring the dimensions of the robot and making sure that the frame was square.
- Cut 2 sets (1 for each color) of 2x4 plywood to the exact size needed to cover the frame perimeter.
- Cut the pool noodles to size and tape them to the outside of the plywood frame.

#### Setbacks
- Creating L brackets of the right size to attach the bumpers to the frame of the robot.
- Aligning the brackets to the mounting points on the robot.
- Ironing on team numbers.
- Rewrapping angles on the bumper with the cloth.

### Drivetrain
#### Purpose
- Make use of a Mecanum drivetrain to allow for increased angles of movement.

#### Build Process
- Assembled Toughbox Classic gearboxes according to instructions from Andymark to convert high rpm, low torque Neo motors to have both medium torque and speed.
- Attach Mecanum wheels to the gearbox, and then the gearbox to the frame.

#### Setbacks
- The wheels had no system to keep them from sliding off the output shaft of the gearbox, requiring a retainment system to be fabricated.
- The Mecanum wheels are fragile, meaning that the rollers on the wheel need to be repaired semi-often.