---
sidebar_label: Logomotion (2011)
sidebar_position: 6
---

# Logomotion (2011)

## Task
[Game Animation](https://www.youtube.com/watch?v=aH_9QHZpsfs)

## Photos

![image](/img/mechanical/logomotion/pic2.png)

<img src="/img/mechanical/logomotion/pic1.png" style={{display: "inline", width: "calc(50% - 5px)", paddingRight: "10px"}} />
<img src="/img/mechanical/logomotion/pic3.png" style={{width: "calc(50% - 5px)"}} />

## Robot Info
Mecanum drivetrain.

Electrical board:

![image](/img/mechanical/logomotion/eboard.png)

## Planning

![image](/img/mechanical/logomotion/cad.png)

[Minibot CAD Model](https://drive.google.com/file/d/1ro2B1OegSoPOtz4s05Bi0K2jhLUMFfMM/view?usp=sharing) produced during offseason (October 2011).