---
sidebar_label: Lunacy (2009)
sidebar_position: 8
---

# Lunacy (2009)

## Task
[Game Animation](https://www.youtube.com/watch?v=ZnGfbGzEFrM)

## Robot Info
See diagram in Planning section.
- Tank drive with 4 wheels powered by two CIMs (one motor per side)
- 28" x 38" x 60" tall
- One-directional shooter powered by two CIMs hooked up to Victor motor controllers
- Roller mechanism - 1 Nippodenso window motor wired to a spike (x2)
- 1 Keyang window motor wired to a spike
- Globe Motor for turret connected to Victor motor controller
- Conveyor mechanism - 2 banebot motors hooked up to Jaguars

## Photo
Team members: see the history document for additional pictures.

<a data-flickr-embed="true" href="https://www.flickr.com/photos/gx67/3419273861/in/dateposted/" title="Warhawks Robotics"><img src="https://live.staticflickr.com/3649/3419273861_48b05ff8db_k.jpg" width="2048" height="1937" alt="Warhawks Robotics"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

*Photo above by former member Wesley Jen*

<a data-flickr-embed="true" href="https://www.flickr.com/photos/team2554/3396980482/" title="NY Robotics 024"><img src="https://live.staticflickr.com/3649/3396980482_4f3a0cf512_o.jpg" width="3072" height="2304" alt="NY Robotics 024"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

## Planning
![image](/img/mechanical/lunacy/Programming_Motor_Outline_2009.bmp)

CAD screenshot (2/1/2009)
![image](/img/mechanical/lunacy/cad.png)