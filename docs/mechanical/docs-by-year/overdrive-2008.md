---
sidebar_label: FIRST Overdrive (2008)
sidebar_position: 9
---

# FIRST Overdrive (2008)

## Task
[Game Animation](https://www.youtube.com/watch?v=D5oL7aLH0T4)

## Robot Info
The robot was named Dingary this year. This was the first robot we ever built (although technically not the first we competed with).

Weighing in at 90 lbs, Dingary was able to lift a 40 inch trackball and place it on the overpass. Over 200 hours were invested in the design, construction and testing of Dingary!

## Photos
[Brunswick Eruption 2008 Album](https://www.flickr.com/photos/team2554/albums/72157608649583833) (yes the album title is wrong)

<a data-flickr-embed="true" href="https://www.flickr.com/photos/team2554/3002063827/" title="DSC_0287"><img src="https://live.staticflickr.com/3038/3002063827_667f6749c5_o.jpg" width="2592" height="3872" alt="DSC_0287"/></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

![img](/img/mechanical/overdrive/comp.jpg)

![img](/img/mechanical/overdrive/image.png)

![img](/img/mechanical/overdrive/pic.jpg)

![img](/img/mechanical/overdrive/building.png)

## Videos
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/95tg17eWJJk?si=5k4_q9uiLlEjSeU_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/RmZkChjQNfc?si=Vm7hIjBaoZsgs1Pu" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Planning
![image](/img/mechanical/overdrive/planning.jpg)

![image](/img/mechanical/overdrive/planning2.png)