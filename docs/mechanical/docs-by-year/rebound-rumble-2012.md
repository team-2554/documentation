---
sidebar_label: Rebound Rumble (2012)
sidebar_position: 5
---

# Rebound Rumble (2012)
We had **two** robots for this season. The first one saw action at the Rutgers competition and the other (named Zenith) competed at Mount Olive.

## Task
[Game Animation](https://www.youtube.com/watch?v=13HpbFjZWto)

## Robot 1
Name unknown.
- Pneumatic brakes that lock robot position
- Accurate shooter capable of turning on x and y axes.
- Shooter uses auto targeting
- Shoots from anywhere on field
- Shoots across full field
- Size: 25”, 36”, 42” tall
- Drive train: 12 wheel, 4 CIM West Coast Alternative drive
- Crosses and balances on bridge

### Pictures

<img src="/img/mechanical/rebound-rumble/r1_front.png" style={{display: "inline", width: "calc(50% - 5px)", paddingRight: "10px"}} />
<img src="/img/mechanical/rebound-rumble/r1_back.jpg" style={{width: "calc(50% - 5px)"}} />

### Videos

<iframe width="1920" height="750" src="https://www.youtube.com/embed/8HOFjU75Q_c?start=103" title="Team 1676 Match 59 @ Rutgers MAR event" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style={{maxWidth: "100%"}}></iframe>

### Construction & Planning

<img src="/img/mechanical/rebound-rumble/r1_chassis.jpg" style={{display: "inline", width: "calc(50% - 5px)", paddingRight: "10px"}} />
<img src="/img/mechanical/rebound-rumble/r1_cad.jpg" style={{width: "calc(50% - 5px)"}} />
<img src="/img/mechanical/rebound-rumble/WhiteBoard.jpg" style={{height: "400px"}} />

## Robot 2 ("Zenith")
Named Zenith.

### Pictures
<img src="/img/mechanical/rebound-rumble/r2_front.jpg" style={{display: "inline", width: "calc(50% - 5px)", paddingRight: "10px"}} />
<img src="/img/mechanical/rebound-rumble/r2_comp.jpg" style={{width: "calc(50% - 5px)"}} />
<img src="/img/mechanical/rebound-rumble/r2_side.png" />