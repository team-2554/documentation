---
sidebar_label: Ultimate Ascent (2013)
sidebar_position: 4
---

# Ultimate Ascent (2013)
Our robot for this season was named Captain Hook, referencing the hook mechanism that was used for climbing the pyramid.

## Task
[Game Animation](http://www.youtube.com/watch?v=wa5MGEZNrf0)

## Robot Info
- 33.5" x 23" x 21" tall
- Drivetrain: 4 wheels, 4 CIMs, Tank Drive
- Autonomous: shoots 3 disks in to 3-point goal
- Shoots into 3-point goal from set middle position
- Climbs pyramid to first level

## Photos

![image](/img/mechanical/ultimate-ascent/showcase.png)

Mr. Kearney and the team fixing the lifting mechanism:

![image](/img/mechanical/ultimate-ascent/fixingHook.png)

Our robot at JP Day during offseason:

![image](/img/mechanical/ultimate-ascent/jpDay.png)

A photo showing the electrical board location.

![image](/img/mechanical/ultimate-ascent/eboard.png)

## Videos
<iframe src="https://drive.google.com/file/d/1VaZcQGmKUqfIenx4NRTi96TGjGdRuXPg/preview" width="640" height="480" allow="autoplay"></iframe>

<iframe src="https://drive.google.com/file/d/134sgTmzeIF0XN9aw7qjJNQAGveenIrAq/preview" width="640" height="480" allow="autoplay"></iframe>

<iframe width="540" height="304" src="https://www.youtube.com/embed/6i3h1C6s6zQ" title="2013 Brunswick Eruption - Qualification Match 17" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen style={{maxWidth: "100%", height: "400px"}}></iframe>

The video below is a recap from the 2013 season that also contains memorable funny moments from that year. Skim through to find testing or competition clips showing that year's robot.
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/qvSHl_UDcuk?si=1ko9IeGbfNw-lUSt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Planning
Final CAD model as of 3/15/2013.

![image](/img/mechanical/ultimate-ascent/CAD3.bmp)

CAD model as of week 5 (2/14/2013).

![image](/img/mechanical/ultimate-ascent/CAD2.jpg)

CAD model as of week 4 (2/2/2013).

![image](/img/mechanical/ultimate-ascent/CAD.jpg)
![image](/img/mechanical/ultimate-ascent/Chasis3.jpg)
![image](/img/mechanical/ultimate-ascent/Chasis4.jpg)
![image](/img/mechanical/ultimate-ascent/Chasis5.jpg)
![image](/img/mechanical/ultimate-ascent/Chasis6.jpg)
![image](/img/mechanical/ultimate-ascent/Chasis.jpg)
