# Drivetrain

---

## Types of Drivetrains

Drivetrains are the most critical part of an FRC robot. It allows the robot to move, and without movement, you can't complete challenges. FRC robots typically use one of five different drivetrains: Tank Drive, Mecanum Drive, Omni Drive, Swerve Drive, and H-Drive.

### Tank Drive

The tank drivetrain is the simplest to build and consists of two tracks with wheels on each side. The robot's movement is determined by how much power is delivered to each track on the robot. If equal power is sent to both tracks the robot will drive forward or backward. If more power is sent to the left side, the robot will turn right, more power to the right will turn the robot left.
<img src="/img/electrical/tankdrive.png" width="400"/>

### Mecanum Drive

Mecanum drive is an omnidirectional wheel design used to move the robot in any direction. It consists of four-wheel, each controlled by an independent gearbox. Each wheel has a set of rollers attached at a 45-degree angle to allow free movement. Mecanum drive is most effective when strafing is an integral part of the robot design. However, by using a mecanum drivetrain, you give up traction, which can be vital depending on the season's challenges.
<img src="/img/electrical/mecanum.png" width="425"/>

### Omni Drive

Omni Drive is similar to mecanum as it has rollers on the wheels; however, the formation of the rollers themselves allows Omni Drive to make sharper turns without needing to turn the entire robot. Each wheel has its speed controller and motor control, and there are three to four wheels on each side of the drivetrain. The downside to using Omni Drive is the lack of traction. Without traction, the robot is unable to get good a grip on the playspace which makes it especially bad for pushing.
<img src="/img/electrical/omnidrive.png" width="425"/>

### Swerve Drive

Swerve drivetrains have individual "modules" on each corner that allow for wheel rotation along two axes. However, it has the downside of being difficult to design/build and program. Each module includes one motor to allow for rotation as well as one motor to actually drive the wheel. Pictured below is a CAD model of an individual swerve module.
<img src="/img/electrical/swervedrive.png" width="400"/>

### H-Drive (Slide-Drive)

H-Drive is structured similarly to tank drive but with another wheel added to the center of the drivetrain. H-Drive primary relies on the five Omni wheels to provide complete movement rather than traction.
<img src="/img/electrical/hdrive.png" width="425"/>
