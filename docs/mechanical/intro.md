---
sidebar_label: Intro
sidebar_position: 1
---

# Mechanical Team Documentation
<hr/>

## Intro
The Mechanical Subteam is in charge of protyping and physically constructing the robot. The mechanical subteam frequently interacts with power tools to fabricate parts for the robot. They also design components such as drivetrains, the chassis, and intakes. Students in this subteam build and design the skeletal structure of the robot, and efficiently arrange and mount the inner mechanisms of the robot to complete tasks.

Under the Documentation by Year category in the left sidebar, you can explore mechanical documentation for our recent robots.