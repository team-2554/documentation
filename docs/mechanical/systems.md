# Systems
- Gearboxes
  - Motors spin at very high speeds but produce very little usable torque (generally).
  - Gearboxes have sets of gears with varying sizes.
  - What do gearboxes do?
    - Gearboxes allow you to change the axis of rotation for a gear
    - They also allow you to modify gear ratios.
    - For example, if you have a 5:1 gear ratio, you'll be able to make it so that by the time one gear spins 5 times, the other spins once.
  - Why is a gearbox needed at all?
    - To change rotation axes
    - Transfers energy from one device to another.
    - Increases torque and reduces speed
- Motors
  - There are several types of motors legal for FRC use:
    - CIMs - fat, black, and really big
      - Used primarily for wheels
      - Bag motors - similar in appearance to CIMs but smaller and slightly less powerful
    - 775 pro - these are very versatile in their use and are almost always used with the versaplanetary gearboxes.
    - REV NEO - these motors are relatively lightweight and compact, allowing for high power density. 
  - Actuators
    - Type of motor that controls a mechanism that is inbuilt.
    - Makes use of motor and threaded axle for one directional movement.
  - Pulleys and Sprockets
    - Pulley: Pulleys reduce load on motors so that components do not have to undergo as much strain.
      - pulleys are used with belts
    - Sprockets: Gears for chains.