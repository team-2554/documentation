# Tools
Be sure to know what these look like! You can find pictures online.

- Hand-drill
  - Can be cordless or have a cord.
  - Attach drill bits for different sizes.
  - Rotates both ways.
- Drill press
  - Bigger version of drill.
  - Drills straight down.
  - Types of drill presses
    - Floor/Standing drill press
    - Bench top drill press
    - Upright sensitive drill press
- Band saw
  - Helps cut irregular shapes.
  - Cuts with more precision.
  - Is a massive band that spins around.
  - Does not go up and down.
- Lathe
  - The part rotates rapidly, with a tool perpendicular to cut.
  - Safety precautions: Wear safety glasses, long pants, closed-toe shoes, tuck back sweatshirt strings, and tie back longer hair.
  - The cross slide moves the lathe along the x-axis.
- Miter saw (Chop Saw)
  - Cuts at angles.
  - Cuts metal.
  - Different blades cut different materials.
  - Safety tip: Hold the trigger, allow the blade to reach maximum speed, and then cut through. Let go and let the blade stop moving before you bring it back up.
  - Types of miter saws
    - Single bevel - Tilts in one direction
    - Double bevel - Tilts in two directions
    - Sliding double bevel - Tilts in both directions and doubles the width of its cut.
- Files
  - Coarse and Fine
    - Coarse
      - Lesser density.
      - Larger particles.
      - Teeth further spaced apart.
    - Fine
      - Higher density.
      - Less particles.
      - Teeth closer together.
- Saws
  - Yellow blade for metal, unpainted for wood.
- Tap and Die
  - Tap makes internal threads.
    - 3 Types of taps: Plug (short chamfer, usually 5 threads),

![](https://i.ibb.co/3pWMHDp/image.png)

   - Taper (longer chamfer, 9-10 threads), 
   - 
![](https://i.ibb.co/Tm6YCsr/image.png)
   - 
   - Bottoming (extremely small chamfer, 2 threads).

![](https://i.ibb.co/Tm6YCsr/image.png)   

  - Recommended to use plug or taper tap first.
  - Die makes external threads.
    - When cutting threads, grind a chamfer onto the object for easier turning.
    - Secure the rod, use lubrication, and make one full turn with the die and tap wrench, then one half turn back.
    - Ensure both the rod and tap wrench are level.
    - Clean the metal chips with a wire brush and check if the desired nut fits on the rod after it's been threaded.
  - Tap and Die Sizing
    - SAE (Society of Automotive Engineers) threads are measured in TPI (threads per inch).
    - Metric threads are measured by the distance between thread crests in millimeters.
    - Thread pitch gauge helps to properly identify thread measurement.
    - Both cut threads and are made of carbon steel.
    - Lubricating the threads aids in minimizing friction.
    - The empty parts on the tap are called flutes and allow the metal bits to fall through.
  - How to tap/thread things: Attach the circular part (die) or the tap, and rotate to cut threads into the screw. When rotating, make one full turn forward and one half turn back.
  - Purpose: Allows grinded screws or messed up threaded inserts to have a more secure connection.
  - Quarter 20 would be 20 TPI, or threads per inch, with the bolt being Â¼ inch in width.
- Rivet gun
  - There are different tips on the rivet gun, each for different sizes of the rivets.
  - Rivet gun tips can be unscrewed and replaced for different sizes needed.
- How to remove a rivet:
  1. Sand down one side, and then the other side will pop out.
  2. Drill into the rivet.
  3. Hammer and chisel
- Rivets are typically made of aluminum or steel.
- The rivet's length should typically be equal to the thickness of the objects you are fastening.

![](https://i.ibb.co/NyyJyNH/image.png)

^ Rivet ^

## Bench Grinder (Pedestal grinder)
- Sharpens and/or polishes metal.
- Usable on any metal tool that needs sharpening.
- The bench grinder typically has 2 wheels.
  - One hard for grinding
  - One soft for polishing
- Most of the wheel is covered, but 1/4th is exposed.
- There is also an eyeshield and work rest attached.
- Spins at 3000-3600 rpm.
- Tongue guard prevents debris from being carried over by the wheel and shot out.
- When using a bench grinder, allow it to reach full rpm.
- Press wheel dresser against the wheel perpendicular to the wheel to square the face of the wheel.

## Misc
- Screws and screwdrivers:
  - Phillips (Plus head)
  - Flathead (Flat head)
  - Allen wrench / hex key
- Hex head (the hexagon shape on top of a standard bolt)
- Know what cross threading and stripped screws are