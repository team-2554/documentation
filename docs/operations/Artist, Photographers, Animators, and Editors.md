﻿---
sidebar_position: 5
sidebar_label: Artist, Photographers, Animators, and Editors
---

# Artist, Photographers, Animators, and Editors

In our team, we need **artists** to compose various artworks that might be used for our branding. Besides brandings, artists often work hand-in-hand in safety animation to help in digital art and creative design. To apply for this position, please make sure that you have submitted a portfolio.

**Photographers** have become a crucial role in our team, especially in maintaining documentation. Prior to last year, very few pictures can outline the history that our team has. Moreover, when constructing slideshows for workshops, awards, etc., photographs are key to showing our impact and who we are. Similar to artists, to apply for this position, please make sure that you have submitted a portfolio.


![Sample1](/img/operations/Robot.jpeg)

![Sample2](/img/operations/Sample1.jpg)

To create visually appealing submissions, we also look for **animators** who have prior knowledge in animating softwares such as Blender. As suggested by the name, the Safety Animation Award only uses animated videos. Animators often work hand-in-hand with artists and video editors to create a creative and unique submission for the Safety Animation Award.

  
Whether it’s to create promotional content or submissions for our awards, we need **video editors**. Creating content requires proper styling and accompanying aesthetic features, so applicants who have this skill are highly encouraged to apply!
