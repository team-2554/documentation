﻿---
sidebar_position: 4
sidebar_label: Awards and Presenters
---

# Awards and Presenters



Team 2554 participates in two major awards:

  

1.  **Impact Award**
  
	As suggested by the name, the award looks for teams who’ve made an impact. In that, they serve as a role-model in their community and actively encourage the youth to delve into STEM. The FIRST Impact Award has a couple of components: documentation, a presentation, executive summaries, and an optional video. To learn more about the Impact award, we highly suggest to explore the following link: [FIRST Impact Award Resources](https://www.firstinspires.org/resource-library/frc/first-impact-award-resources)

  

2.  **Safety Animation Award**
    
	The safety animation award calls teams to creatively promote a safe environment according to a theme, while emphasizing specific values like conservation of energy or creativity. To learn more about the Safety Animation Award, we highly suggest exploring the following link: [FIRST Safety Animation Award.](https://www.firstinspires.org/robotics/frc/safety)

A few presenters (generally 2-3) are selected to present the Impact presentation, which is hosted on the day of the competitions we attend. Besides the Impact Award, presenters are highly needed for our workshops presented at various hackathons. In previous years, we’ve collaborated and presented workshops at HappyHacks, GooseHacks, MetroHacks, and more allowing us to obtain an international presence.
