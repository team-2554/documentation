﻿---
sidebar_position: 7
sidebar_label: EMERGE
---

# EMERGE

EMERGE stands for "Edison-Metuchen Educational Robotics Growth Environment". It was originally an idea formed by leaders within our team and an external FTC team, Dynabytes. Unlike most other programs described here, EMERGE is not a program that is necessarily controlled by our team. Instead, EMERGE is intended to be a sort of "student council" of FRC, FTC, and other robotics teams/groups specifically in the Edison-Metuchen area. In the short term, this could lead to some collaborative outreach events, such as robot demos and showcases, but we hope in the very long term that EMERGE could lead to real FLL, FTC, and/or FRC offseason events being held in our community through the collaborative efforts of the various robotics groups in the area.

  

EMERGE has a draft constitution in the form of bylaws available [here](https://docs.google.com/document/d/1G2Q_1kJFeATVh7DEhpGMcvxsvCZaaF-MloMv_I7ZWH4/edit?usp=sharing). The document is quite long because it covers nearly every detail of a variety of scenarios; the most important sections are Mission, Foreword, and Management. Reading just those three sections will provide a good enough understanding of what EMERGE intends to be.

  

While EMERGE is a big vision, not much has happened yet with regard to implementing the idea, largely because there is just too little activity in the community. Many local teams have joined the EMERGE Discord server or expressed interest, but very few have actually appointed representatives, which has prevented the appointment of a corresponding secretary or the serious discussion of any collaborative projects. Our previous Operations Manager has suggested having an in-person get together event with all prospective member teams and other nearby teams being invited to discuss EMERGE and increase interest.
