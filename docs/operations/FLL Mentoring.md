﻿---
sidebar_position: 6
sidebar_label: FLL Mentoring
---

# FLL Mentoring


Mentoring younger children is a major component of our outreach initiatives. Most FRC teams, like ours, mentor FLL teams. FLL stands for FIRST Lego League, and focuses on introducing younger children to robotics through Lego. There are different FLL programs for different age groups, but the two which we are involved in are FLL Explore (ages 6-10) and FLL Challenge (ages 9-16).

  

Right now, we are only currently mentoring a small community-based FLL Explore team called Da Vinci Bots, but for many years until the pandemic we mentored JMI's official FLL Challenge team, the RoboKnightmares.

  

Expanding and improving our FLL programs is very important to us; not only do they raise awareness of FIRST programs and STEM among young children in our community, but they also give us more material to work with for important award and grant applications.

  

Learn more about FLL here:

-   [https://www.firstlegoleague.org/](https://www.firstlegoleague.org/)
    
-   [2024 FLL Challenge Resources](https://www.firstinspires.org/resource-library/fll/challenge/challenge-and-resources)
    
-   [2024 FLL Explore Resources](https://www.firstinspires.org/resource-library/fll/explore/challenge-and-resources)
