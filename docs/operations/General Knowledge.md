﻿---
sidebar_position: 2
sidebar_label: General Knowledge
---

# General Knowledge

Whether you are a grant writer or an artist, something you’ll find necessary in our team is team history. We only started documentation last year, but our team is considered a veteran team since we have 15+ years of rich history. To learn more about the team’s defining moments in its history and more about the past couple of years, visit the team history document attached [here](https://docs.google.com/document/d/1TbldS09Ly6Ykazo2U9TZ8TCzBvBXQYJKQ8O9XfAJB4o/edit?usp=sharing).


At competitions, we often have to work with and speak to other teams. In fact, during the competition, teams work together in **alliances**, generally each qualification match having one red alliance and one blue alliance. Alliances are generally randomized during the qualification matches, however during the alliance selection process, teams must choose other teams. To learn more about alliances, it is HIGHLY suggested you view the following video: [Alliance Selection Process](https://www.youtube.com/watch?v=Nnac20aFjeI). When it comes to forming alliances during playoffs, we need to make informed decisions using data from **scouting**.  Generally, we scout in two different ways. While the match is ongoing, we track how the match plays out to learn about how the robot is moving and controlled which is known as **match scouting**. Information we can’t learn from simply watching the match, such as specifics of the robot like dimensions and parts is what we learn from **pit scouting**.

  

Throughout the year, we generally have three main competitions: Brunswick Eruption (off-season) and 2 competitions in March which is when we play the game for that year. During the latter 2 competitions, we are given the opportunity to present for the Impact Award.  


