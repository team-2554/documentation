﻿---
sidebar_position: 3
sidebar_label: Grant Writers
---

# Grant Writers


In the Operations subteam, writing is an immensely useful and needed skill. To simply register for the team, we need **$10,000 or more!** Reaching out to sponsors and grants are crucial to ensure the team is well funded. Beyond the scope of writing applications for grants and sponsorship inquiries, Operations writers can often play instrumental roles in publishing media and writing scripts for the awards we present, namely the Safety Animation Award and the Impact Award.

  
Some of our sponsors from last year include: Olight, EMS Marcon, and more which are listed in the sponsors tab of our website ([jpsrobotics2554.org](https://jpsrobotics2554.org/sponsors/)).


