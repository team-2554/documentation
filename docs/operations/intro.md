﻿---
sidebar_position: 1
sidebar_label: Intro
---

# Intro

If you have not already, we highly recommend you to visit [firstinspires.org](https://www.firstinspires.org/about#:~:text=The%20FIRST%20Core%20Values%20are,equity%2C%20diversity%2C%20and%20inclusion.), which will serve as a proper introduction to FIRST as a general organization. For Operations, we want to highly emphasize the core values of FIRST: discovery, innovation, impact, inclusion, teamwork, and fun.

  

Operations looks for specialized skills that could better improve the branding, funding, and several other crucial aspects of Team 2554. To learn about each of the specialized roles, keep reading on!
