---
sidebar_position: 2
sidebar_label: Getting Started
---

# Getting Started

Here, we'll walk you through the steps of setting up a development environment on your school macbook so you can start working on personal projects/take home task for operations programming. 

## Download an IDE

Any IDE suited for front end development can be used here, however we are going to install vs code due to its easy to use and versatile features compared to more specific ihdes.

1. Download Visual Studio code from there website, you can find the link [here](https://code.visualstudio.com/download). The reason we don't use self service here is that it does not have the latest version of VS code and could impact how much we can do (the version that self service has is extremely old). Make sure to download the macOS version.

2. Open up the zip file in your downloads folder and drag Visual Studio Code onto your desktop or prefered location for the .app file to be executable.

### installing VsCode CLI (optional)

If you want vscodes command line interface follow below (makes parts of development easier):

1. Download the Visual Studio code from their website [here](https://code.visualstudio.com/download). Then move the "code" CLI file into your bin folder. You can follow these commands to make it easier, make sure you know where your file is located on your computer.

```bash 
# make a bin folder in your root if you don't have one 
mkdir -p ~/bin
mv /path/to/code/file ~/bin/

nano ~/.zshrc 
```

2. Now you can add the CLI to path by typing out:

```bash
export PATH = "$HOME/bin:$PATH"
```

3. Now press ctrl+x and enter to exit out of the editor

4. reload your .zshrc file , you can do this by doing: 

```bash 
source ~/.zshrc
# check if correctly added to PATH 
code --version
```

Now you have the CLI (command line interface) installed!

## Downloading Git 

Git is an essential tool that should be used at all times when updating, reviewing, or building new code. 

1. Install "Xcode" from self service and make sure to install the "Xcode Post install script 2023" as well after Xcode is installed so that everything works. 

![drawing](/img/ops-prog/getting-started/image1.png)

2. You can look at the git resources in the programming section [here](/docs/programming/external-resources.md). It is essential that you have a good understanding of git to be on the subteam.

## Downloading Node Js

Node JS is a javascript runtime that we use to build web apps.

1. Install the Node Js prebuilt binaries from [here](https://nodejs.org/en/download/prebuilt-binaries). Make sure you select MacOS and ARM64 for your options.

![drawing](/img/ops-prog/getting-started/image2.png)

2. Now install and open the tar file on your computer to your desired location. Then you need to add the source file to your PATH. Open up a terminal instance on Vscode by using the top menu. Once you have your terminal open, type this command in to edit your PATH:

```bash
nano ~/.zshrc
```

3. Now that you have the editor open, add to PATH using this command. Make sure you know the path from the root directory of the Macbook to the bin folder in your node folder that you have perviously installed.  

```bash
export PATH="$HOME/path/to/bin/folder:$PATH"
```

4. Now press ctrl+x + enter to exit the editor. You can test if your node is correctly added to PATH though this command:

```bash
node -v
```

You are now ready to start making svelte apps with node js on your school macbook! Please see the svelte tutorial [here](/docs/ops-prog/svelte.md)
