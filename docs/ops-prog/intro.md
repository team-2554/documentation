---
sidebar_position: 1
sidebar_label: Intro
---

# Operations Programming Subteam

Yes, Operations has its own Programming subteam. Why?

- Creating software tools for the Build team (e.g. FRC scouting app, inventory management system)
- Updating and designing the team's website 
- General automation tasks (e.g. merch sale automation, hackathon research automation)
- Data processing
- Writing code for outreach projects (including smaller robots)

These might sound like fairly uncommon needs, but programming talent is necessary in Operations more than you might expect. Additionally, tasks that traditionally were tasked with the build team, such as updating the website, are also included here.

## Whats the difference?

The build programming subteam codes our robot using WPI Lib in java, which requires a different set of skills compared to what the operations programming subteam will be tasked with. Members will be expected to code web apps and google apps scripts (GAS), which are mostly in javascript. In general, experience in front-end development is preferred on this subteam.

## What tools do we use?

Although most of our projects are done in Svelte, a javascript framework, experience in the following frameworks is also prefered as transfering skills from one frame work to another is straightforward: 

- React
- Angular JS
- Vue

## How do I get involved?

This year, applicants get to choose to apply to operations programming or build programming. Both sub-teams require skilled programmers to succeed and we encourage applicants to apply to both!

Applicants can prepare by working on personal projects with the technologies that the operations programming team uses. Take a look at [Getting Started on School Macbook](/docs/ops-prog/getting-started.md)

## Resources
External resources are linked throughout the documentation when relevant.

- For Git-related resources, see the [External Resources](/docs/programming/external-resources.md) page under the Build Programming section