---
sidebar_label: Past Projects
---

# Past Projects

Listed below are some past (and ongoing) projects and tasks given to members of the operations programming team.

### FRC Pit Scouting App (Svelte)

In FRC, a team is paired with others to form a larger team called an alliance. While initially these alliances are formed at random, during playoffs teams get to select who the ally with. Crucial information about a team's robot is required in order to make a proper alliance selection. This is what scouting is: getting information from teams to make informed alliance picks. Pit scouting allows for a team to get specific information about other robots that otherwise wouldn't come up in match scouting.

In past years, the team has experimented with different ways to efficiently pit scout, such as a google form or writing information down. However both of these approaches have their problems, such as the lack of network in pits and non-standardized information. This project aims to solve both, letting users answer specific questions about other robots and allowing for peer to peer sharing, essentially a google form that doesn't need an internet connection. 

### Merch Automation (GAS)

Handling merch orders can be a difficult task, as you have to keep everything organized in order to ensure that all orders are fulfilled/paid for. This projects goal is to allow the team to, in a matter of clicks, check the order status, payment status, and email students about their own order information (how much their owed, ordered merch, etc).

This project was developed with google apps script and is linked to a spreadsheet.

### Inventory Management System (GAS + Svelte)

There are a lot of parts, big and small, that tend to get misplaced. Creating an inventory management system is an easy way for team members to access the location of parts and update them accordingly.

This project involves a Svelte web app thats built using Google Apps Script thats also connected to a spreadsheet for reference. The web app contains an easy to use interface that automatically updates the spreadsheet based on user inputs. 