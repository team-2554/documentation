---
sidebar_position: 3
sidebar_label: Svelte 
---

# Svelte

A javascript framework is essentially a tool thats used for web development and makes our lives as devs alot easier. Frameworks simplify complex tasks like manipulating DOM elements, managing state and routing. 

Svelte is one of many javascript frameworks, but it is the one that our team uses.

## Why use Svelte? 

In addition to being faster than other frameworks (due to the absence of a virtual DOM), Svelte offers a clean, component based experience, with other features that make developing websites much easier. For example, an in-built reactivity system allows for code that doesn't have to go through state management, leading to variables being tracked and updated to the UI as they updated themselves.

This makes developing in Svelte simpler and more intuitive.

### Getting Started

A fairly robust tutorial for Svelte can be found on their official website [here](https://learn.svelte.dev/tutorial/welcome-to-svelte)