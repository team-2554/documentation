---
sidebar_label: PID and Motion Profiling
sidebar_position: 1
---

# PID and Motion Profiling

---

### PID

PID stands for proportional-integral-derivative. It's a form of closed loop control in order to compensate for inconsistencies in the motor and optimize when and how to reach and maintain the desired motor setpoints.

In our team, we generally use custom PID with our Spark Max controllers from REV Robotics. Simply put, we tune the PID of these motors by plugging in the motors to our computer and opening the REV Client software. We then run the motor at a setpoint and adjust the various PID values in order to achieve the optimal gain and error values. 

<img src = "https://2589213514-files.gitbook.io/~/files/v0/b/gitbook-legacy-files/o/assets%2F-M7iEUG41QXEdyOMqbkc%2F-MQTHLKYFA25LK2C9o5m%2F-MQTWb4WCTBJPxvGQGLE%2FSPARK%20MAX%20-%20Run%20(modified).svg?alt=media&token=e0d97d44-4b48-4416-8c4f-af9940222965"></img>

[Here is an article explaining the detailed math behind PID](https://docs.wpilib.org/pt/latest/docs/software/advanced-controls/introduction/index.html). 

### Motion Profiling

Motion Profiling, aka Path Planning is a way to make the robot accurately follow preplanned paths, and this is mostly used for games which have an autonomous section. I really don't feel like explaning it and to be honest I don't really know much about motion profiling, so just follow the guide on the [WPILib website](https://docs.wpilib.org/en/latest/docs/software/pathplanning/index.html).

One of the things to remember when doing motion profiling is that when you do get the PID parameters from the Robot charectarizer, you can program them into the Talon SRX motor controller instead of running the PID loop on the RoboRIO. Though this is much more complicated to do, it might yeild better performance. However, if and when you do motion profiling, getting it working should be the first priority, and only after you have properly working motion profiling do you focus on moving the PID processing to the Talon SRXs(Talon SRX is one of the advanced motor controllers that is connected through the CAN bus).
