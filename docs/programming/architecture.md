---
sidebar_label: Architecture
sidebar_position: 3
---

# Architecture

---

### Network Architecture

The RoboRIO is like the brain of the robot. It's the central point for all input/output for the robot. The RoboRIO is connected to a radio through Ethernet, which usually acts as a wireless access point, but can also connect to another AP, which is usually done at competitions.

The coprocessor (Raspberry Pi or Jetson) is also attached to the radio through Ethernet.

While not at competitions, the driver station computer connects to the AP created by the robot radio, and an application on the driver station computer allows the driver to send control inputs to the RoboRIO. At competitions, the driver station is connected to an ethernet port, and the field computer does some weird networking magic to link it to the robot.

Like described before, the NetworkTables library can be used to communicate between devices connected to the robot radio.

NetworkTables is also used to transmit data back to the driver station. Information such as sensor data and other diagnostic data can be sent this way.

More information on software components can be found [here](https://docs.wpilib.org/en/stable/docs/controls-overviews/control-system-software.html). 

### Hardware Architecture

Motor controllers are connected to the RoboRIO through a variety of connectors. Basic motor controllers are connected through PWM pins. PWM is a way of encoding analog signals as digital signals, and it basically allows the RoboRIO to send a 0-100% number to the motor controller. More info on PWM [here](https://www.arduino.cc/en/Tutorial/Foundations/PWM). However, we hardly use simple PWM motors anymore.

More advanced motor controllers are connected through the CAN bus. The CAN bus is an interface which allows connected multiple devices over only 2 wires (High and Low), and allows commands to be sent to each device individually. Some of our advanced motor controllers such as the Spark MAX's are connected throuh the CAN bus. However, using the CAN bus allows for more advanced features, such as running PID loops onboard a motor controller. 

Most commons sensors will have a class in the WPILib library for easy control. Basic sensors are connected through the digital input/output ports. More advanced sensors might sometimes use the I<sup>2</sup>C interface.

Some years, we also use pneumatics. Pneumatic solenoids are connected to a Pneumatic Control Module, which is in turn connected to the RoboRIO through the CAN bus. WPILib has easy to use libraries to control solenoids and other pneumatic-related devices.

Here's a wiring diagram that shows how everything is connected. Focus on the wires that are used for signaling. 
<img src="https://docs.wpilib.org/en/stable/_images/frc-control-system-layout.svg" style={{backgroundColor: '#ffffff80'}}></img>

### Software Architecture

In Team 2554, we use the command-based programming architecture to program our RoboRIO. 

In order to control the robot through a laptop, we use the Driver Station, which you can learn about [here](https://docs.wpilib.org/en/stable/docs/software/driverstation/index.html). Driver Station allows us to enable and disable our robot, pair controllers, view button mappings, and debug logs on our robot. Make sure to at least look at the general overview of the Driver Station [here](https://docs.wpilib.org/en/stable/docs/software/driverstation/driver-station.html). This will be important when testing and deploying code to our robot.

We also use Glass to view any data put on NetworkTables. Glass allows us to easily visualize raw and processed data from Network Table through various widgets. You can learn about Glass [here](https://docs.wpilib.org/en/stable/docs/software/dashboards/glass/introduction.html).

The WPILib Java API documentation [here](https://github.wpilib.org/allwpilib/docs/release/java/index.html) provides a general overview of all the packages and method names we can use to program our robot. It is helpful as a quick reference to find any method and what it does. 

AdvantageKit is a logging, telemetry, and replay framework developed by Team 6328 for robotics. It allows the full state of robot code to be replayed in a simulation using log files and helps us troubleshoot and optimize performance. It integrates with WPILib and allows us to write general hardware agnostic implementations of subsystems. You can learn about AdvantageKit [here](https://github.com/Mechanical-Advantage/AdvantageKit).

We use the Advanced Swerve MonoRepo as our starting point in the AdvantageKit framework, located [here](https://github.com/Mechanical-Advantage/AdvantageKit/releases).

PathPlanner is a powerful path planning tool developed by Team 3015. It enables teams to create motion profiles using Bézier curves, fine-tuning robot movements for complex tasks and allows us to trigger events along paths, helping us simplify complex auto routines. It comes natively with AdvantageKit, with basic support already integrated in the monorepo. You can learn about PathPlanner [here](https://github.com/mjansen4857/pathplanner).

We used PathPlanner this year for our auto routines and on-the-fly path generation.


### Command Based Programming

A subsystem in our robot code represents a specific part of our robot which contains methods that manipulate the actual hardware on the robot to do something. An example of a subsystem can be a DriveTrain or an Arm on the robot.

A command in our robot code represents an action which calls a specific function in our subsystem in order to tell the subsystem to do something. For example, if we have a moveArmUp() function in our subsystem which moves the arm up, we can create a command called MoveArmUp, which calls the method in the subsystem and schedules it to be run.

The benefit of this style of programming is that it allows users to easily define robot actions and components without having to write any complicated logic. It allows our code to be easily understandable and easily debuggable.

To learn more about the command based programming we use, please look [here](https://docs.wpilib.org/en/stable/docs/software/commandbased/what-is-command-based.html) or click on Repositories and FRC-2020 to see an explanation of this architecture on our robot.