---
sidebar_label: External Resources
sidebar_position: 2
---

# External Resources
This page contains a variety of documentation and tutorial materials not produced by our team that may be useful to you.

---

## Official Documentation

There is plenty of documentation available online for all of the libraries and technologies we use. You can Google any of the terms mentioned in our documentation and you will find results. Here is some recommended reading:

- The official [WPILib documentation](https://docs.wpilib.org/en/stable/index.html) is the most important resource available to you. WPILib is the library used to code FRC robots. This site also contains documentation for topics relevant to other subteams.
  - You may see examples for C++ alongside Java examples. We do not use C++! We only use Java with WPILib (and Python for coprocessors).
  - See also the WPILib [Java API reference](https://github.wpilib.org/allwpilib/docs/release/java/index.html) if you are writing actual robot code.
- GitLab's [official tutorial](https://docs.gitlab.com/ee/tutorials/) both for Git and GitLab itself.
- OpenCV's [official documentation](https://docs.opencv.org/4.x/). We usually use Python when using OpenCV, but the same concepts can be applied to C++ or Java.
- [PhotonVision documentation](https://docs.photonvision.org/en/latest/)
- [Limelight documentation](https://docs.limelightvision.io/)
- [AdvantageKit documentation](https://github.com/Mechanical-Advantage/AdvantageKit/blob/main/docs/WHAT-IS-ADVANTAGEKIT.md)

## Videos from Other Teams

**DISCLAIMER**: Content from other teams was found publicly online and is in no way affiliated or maintained by Team 2554. We extend our gratitude to these teams for publishing these resources. Team 2554 members and applicants should bear in mind that other teams **may install or use software slightly differently** than we do on our school devices. Please make sure to read through our own documentation first so you recognize the differences.

Many teams publish video series to help their own new members learn FRC programming. They are often public resources and we highly recommend researching them online. A few are linked here:

- [Team 1923's Programming Playlist](https://www.youtube.com/watch?v=pcUj3GT_1LU&list=PLjuLcdW1ogBSBB6K4n_JxLnw_nst5V_Us&pp=iAQB)
- [Team 5070's Robotics Crash Course](https://www.youtube.com/watch?v=cktxIMSLC1M&list=PLJiHFUB7JbJSvRJDe9abMhz5sKL_UxYCr&pp=iAQB) (Videos for all subteams including programming)

## Videos from Online

Some other videos or playlists covering relevant topics, not specific to FRC:
- [Java in 9 Hours](https://www.youtube.com/watch?v=grEKMHGYyns) by freeCodeCamp.org
  - Note that there are many other skills besides Java knowledge that are necessary in our programming subteam.
- [Python & OpenCV in 4 Hours](https://www.youtube.com/watch?v=oXlwWbU8l2o) by freeCodeCamp.org
- [What is Git?](https://www.youtube.com/watch?v=2ReR1YJrNOM) by Programming with Mosh
- [Git in 1 Hour](https://www.youtube.com/watch?v=8JJ101D3knE) by Programming with Mosh
- [GitLab Tutorial Playlist](https://www.youtube.com/playlist?list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_) by Automation Step by Step
  - Not everything in this tutorial is a requirement. Essential Git skills are really what we need, but everything else is helpful too.