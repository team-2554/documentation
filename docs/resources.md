# External Resources
A collection of external resources that aren't specific to any one subteam. Subteam-specific external resources can be found within the relevant pages. 

---

## Official FIRST Resources
Game manuals and rules change every year, so search online for the most recent materials.

- [FIRST Website](https://www.firstinspires.org/)
  - [FRC Website](https://www.firstinspires.org/robotics/frc)
- [FRC Awards Page](https://www.firstinspires.org/robotics/frc/awards)
- [WPILib Documentation](https://docs.wpilib.org/en/stable/index.html)
  - Focused mainly on programming, but contains information relevant to other build subteams as well.
- [FMA Website](https://midatlanticrobotics.com/) - The website for our FRC district.

## Online FIRST Communities
Most of these are not affiliated with FIRST. Apply common sense and follow safe practices when communicating with strangers online (or avoid doing so altogether).

- [Official FIRST Forum](https://forums.firstinspires.org/) - The only "official" FIRST/FRC discussion forum, although less popular than the others listed below.
- [Chief Delphi](https://www.chiefdelphi.com/) - The most widely used discussion forum for all things FIRST.
- [FRC Subreddit](https://www.reddit.com/r/FRC/) - The FRC community of Reddit.
- [Unofficial FIRST Discord Server](https://discord.gg/frc) - The largest FIRST community on Discord.
- [FIRST Updates Now Discord Server](https://discord.gg/firstupdatesnow) - Another large FIRST Discord server managed by the FIRST commentary/reporting group FIRST Updates Now (FUN).
- [FMA Discord Server](https://discord.gg/epTfCBc9ZT) - The Discord server for teams in our FRC district.

If you are not yet a member of Team 2554, please do not claim to be on any online FIRST communities. You are welcome, however, to discuss with other people about preparing to apply or anything else about FIRST/FRC.

## Third-Party Resources
These may be produced by other teams or organizations. We are not affiliated in any way with these groups, but we extend our gratitude to them for publishing their resources online.

- [The Blue Alliance](https://thebluealliance.com) - A public database of information on all FRC teams and events. Widely used for researching specific teams or reviewing match/event data.
- [Statbotics](https://statbotics.io) - A public database and analysis service for team performance data. Often used during scouting for alliance selection or simply to measure improvement.
- [Team 5070's Robotics Crash Course](https://www.youtube.com/watch?v=cktxIMSLC1M&list=PLJiHFUB7JbJSvRJDe9abMhz5sKL_UxYCr&pp=iAQB) (Videos for all build subteams)
