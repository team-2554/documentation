// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const math = import("remark-math");
const katex = import("rehype-katex");

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Team 2554 Documentation",
  tagline: "Official FRC Robotics Team of John P. Stevens H.S. in Edison, NJ",
  favicon: "img/favicon.ico",

  // Set the production url of your site here
  url: "https://docs.jpsrobotics2554.org",
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: "/",

  // GitHub pages deployment config.
  // If you aren't using GitHub pages, you don't need these.
  organizationName: "Team 2554", // Usually your GitHub org/user name.
  projectName: "documentation-v2", // Usually your repo name.

  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",

  // Even if you don't use internalization, you can use this field to set useful
  // metadata like html lang. For example, if your site is Chinese, you may want
  // to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: "en",
    locales: ["en"],
  },

  presets: [
    [
      "classic",
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
          editUrl:
            "https://gitlab.com/team-2554/documentation-v2/-/tree/master",
          routeBasePath: "/",
          remarkPlugins: [math],
          rehypePlugins: [katex],
          showLastUpdateTime: true,
          showLastUpdateAuthor: true,
        },
        // blog: {
        //   showReadingTime: true,
        //   // Please change this to your repo.
        //   // Remove this to remove the "edit this page" links.
        //   editUrl:
        //     'https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/',
        // },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
        gtag: {
          trackingID: "G-QQN46EJ7ST",
          anonymizeIP: false,
        },
      }),
    ],
  ],
  stylesheets: [
    {
      href: "https://cdn.jsdelivr.net/npm/katex@0.16.8/dist/katex.min.css",
      type: "text/css",
      integrity:
        "sha384-GvrOXuhMATgEsSwCs4smul74iXGOixntILdUW9XmUC6+HX0sLNAK3q71HotJqlAn",
      crossorigin: "anonymous",
    },
  ],
  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: "img/docusaurus-social-card.jpg",
      navbar: {
        title: "Team 2554 Documentation",
        logo: {
          alt: "Team 2554 Logo",
          src: "img/warhawks_logo.png",
        },
        items: [
          {
            href: "https://gitlab.com/team-2554/documentation-v2",
            label: "GitLab",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Social",
            items: [
              {
                label: "Our Website",
                href: "https://jpsrobotics2554.org/index.html",
              },
              {
                label: "Instagram",
                href: "https://www.instagram.com/jpsrobotics2554/",
              },
              {
                label: "Blue Alliance",
                href: "https://www.thebluealliance.com/team/2554",
              },
            ],
          },
          {
            title: "Code",
            items: [
              {
                label: "Github",
                href: "https://github.com/team2554",
              },
              {
                label: "Gitlab",
                href: "https://gitlab.com/team-2554",
              },
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Team 2554. Built with Docusaurus.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
